import math

def fibonacciFormula(a):
  
  Phi = (1 + math.sqrt(5)) / 2
  phi = (1 - math.sqrt(5)) / 2
    
  return ((Phi ** a) - (phi ** a)) / math.sqrt(5)
  
numLoop = input("How many Fibonacci numbers? ")

for i in range(0, numLoop):
  
  print fibonacciFormula(i)


